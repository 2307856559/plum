package com.plum.session;

import javax.servlet.http.HttpSession;

/**
 * Session工具类
 * @author lijiahong
 * @createDate 2018年1月22日10:48:28
 */
public class PlumSession {
	
	/**
	 * 获得session
	 * @return
	 */
	public static HttpSession getSession(){
		if(HttpContext.get()!=null){
			return HttpContext.get().getSession();
		}
		
		return null;
	}
	
	public static Object getAttr(String name){
		HttpSession session = getSession();
		if(session !=null){
			return session.getAttribute(name);
		}
		return null;
	}
	
	public static void setAttr(String name,Object val){
		HttpSession session = getSession();
		if(session !=null){
			session.setAttribute(name, val);
		}
	}
	/**
	 * 设置session超时时间
	 * @param sec 单位秒,设置为-1永不过期
	 */
	public static void setMaxInactiveInterval(int sec){
		HttpSession session = getSession();
		if(session !=null){
			session.setMaxInactiveInterval(sec);//设置单位为秒，设置为-1永不过期
		}
	}
	
	public static void removeAttr(String name){
		HttpSession session = getSession();
		if(session !=null){
			session.removeAttribute(name);
		}
	}

}
