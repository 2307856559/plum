package com.plum.realm;

import java.util.Map;
import java.util.Set;

/**
 * 抽象PlumAbstractRealm类 允许用户自由扩展和登陆认证、权限设置
 * 
 * @author lijiahong
 * @CreateDate 2018年1月23日12:51:02
 */
public abstract class PlumAbstractRealm {
	/**
	 * 登录认证规则扩展
	 * 返回null代表不扩展
	 * @return
	 */
	public abstract Map<String, String> AllowMapExt();

	/**
	 * 权限认证规则扩展
	 * 返回null代表不扩展
	 * @return
	 */
	public abstract Map<String, Set<String>> roleMapExt();

	/**
	 * 登录认证
	 * 返回null代表登录成功,返回对象认为登录成功
	 */
	public abstract Object loginAuth(Map<String, Object> param);

	/**
	 * 设置当前用户拥有的权限
	 * @return
	 */
	public abstract Set<String> setHaveRoles();
}
