package com.plum.exception;

/**
 * 表达式解析错误异常
 * 
 * @author lijiahong
 * @CreateDate 2018年1月24日09:06:49
 *
 */
public class PlumParseException extends Exception {
	private static final long serialVersionUID = -7803779686351478290L;

	public PlumParseException() {
		super();
	}

	public PlumParseException(String message) {
		super(message);
	}
}
