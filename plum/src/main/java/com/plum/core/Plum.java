package com.plum.core;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Plum {
	public final static String PLUM_ALLOW = "allow";
	public final static String PLUM_DENY = "deny";
	public final static String PLUM_ROLES = "roles";
	public final static String PLUM_INI_PATH = "iniPath";
	public final static String PLUM_LOGIN_URL = "loginUrl";
	public final static String PLUM_UNAUTHORIZED_URL = "unauthorizedUrl";
	public final static String PLUM_REALM = "realm";
	public final static String PLUM_DEFAULT_ENCODING = "UTF-8";
	public static String DEFAULT_LOGIN_URL = "/login";
	public static String DEFAULT_UNAUTHORIZED_URL = "/unauthorized";
	public static String SESSION_USER_KEY = "PLUM_SESSION_USER_KEY";
	public static String SESSION_ROLES_KEY = "PLUM_SESSION_ROLES_KEY";
	public static String REALM_CLASS = "";

	protected static Map<String, String> allowMap = new HashMap<String, String>();
	protected static Map<String, Set<String>> roleMap = new HashMap<String, Set<String>>();
	protected static Map<String, String> urlRegixCacheMap = new HashMap<String, String>();

	public static Map<String, String> getUrlRegixCacheMap() {
		return urlRegixCacheMap;
	}

	public static void putUrlRegixCacheMap(String key, String val) {
		urlRegixCacheMap.put(key, val);
	}

	public static Map<String, String> getAllowMap() {
		return allowMap;
	}

	public static void setAllowMap(Map<String, String> map) {
		allowMap.clear();
		allowMap.putAll(map);
	}

	public static void putAllowMap(Map<String, String> map) {
		allowMap.putAll(map);
	}

	public static void putAllowMap(String key, String val) {
		allowMap.put(key, val);
	}

	public static Map<String, Set<String>> getRoleMap() {
		return roleMap;
	}

	public static void setRoleMap(Map<String, Set<String>> map) {
		roleMap.clear();
		roleMap.putAll(map);
	}

	public static void putRoleMap(Map<String, Set<String>> map) {
		roleMap.putAll(map);
	}

	public static void putRoleMap(String key, Set<String> val) {
		roleMap.put(key, val);
	}

	public static void addRoleMap(String key, String val) {
		if (roleMap.containsKey(key)) {
			roleMap.get(key).add(val);
		} else {
			Set<String> set = new HashSet<String>();
			set.add(val);
			putRoleMap(key, set);
		}
	}
}
