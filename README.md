#

###  plum

非常灵活、简单、轻量的java权限管理开源项目，能让你轻松上手并控制好所有权限 **plum** （体积仅有18k）

plum具有以下功能
- 登录认证（通过ini文件配置和自由扩展获得允许直接访问的url，如资源文件和业务相关action等，其余url则必须登录后访问）
- 权限认证（能轻松的自由扩展权限限制，简要来说就是用户是否拥有该url的访问权限，没有则跳转到配置的无权限页）
- session管理（从程序任何地方轻易的对session进行读取和操作）
- 《待续》
### plum入门

1. 引入plum（打包生成plum.jar或通过maven引入，不过不好意思maven还未提交，暂时不能使用,附件里有jar包和资源包）
```Java
    <dependency>
      <groupId>com.plum</groupId>
      <artifactId>plum</artifactId>
      <version>1.0.0</version>
    </dependency>
```

2. 在web.xml中配置filter（plum需配置为第一个filter）
```
	<filter>
		<filter-name>plum</filter-name>
		<filter-class>com.plum.filter.PlumFilter</filter-class>
		<init-param>
			<param-name>iniPath</param-name>
                        <param-value>src/main/resources/plum.ini</param-value>
		</init-param>
	</filter>
	<filter-mapping>
		<filter-name>plum</filter-name>
		<url-pattern>/*</url-pattern>
	</filter-mapping>
```

3. 新建plum.ini文件并配置

```
#登录页面地址
loginUrl=/login
#未授权页面地址
unauthorizedUrl=/html/error/unauthorized.html

#自定义realm地址
realm=com.shouxun.config.realm.PlumRealm

# *号代表所有
#配置在未登录下允许通过的url，未配置一律需要登录后访问
/login/*=allow
/assets/*=allow
/html/*=allow

#url需要哪些权限访问
/admin/*=roles[admin,test]
```


4.新建类PlumRealm并实现PlumAbstractRealm方法,以下是实际项目中实现例子，例子使用的jfinal3.3，仅供参考（记得修改plum.ini中的realm地址^-^）
```java
//重点，实现PlumAbstractRealm的4个方法
public class PlumRealm extends PlumAbstractRealm{
	private UserProvider userService = new UserProviderImpl();
	/**
         *登陆访问规则扩展，返回null代表不扩展，扩展的是在plum.ini中的allow列表基础上在扩展
         */
        @Override
	public Map<String, String> AllowMapExt() {
		// TODO Auto-generated method stub
		return null;
	}
        
        /**
	 * 需根据自己业务实现登录认证
         * 返回null代表登录失败，不为null代表登录成功，这里返回的对象可以通过PlumSecurity.getUser()得到
	 */
	@Override
	public Object loginAuth(Map<String, Object> param) {
		String userName = (String)param.get("userName");
		String password = (String)param.get("pwd");
		UserInfo user = userService.getUser(userName);
		if(user == null)
			return null;
		if(password.equals(user.getPwd())){
			//缓存当前部门
			PlumSecurity.setAttr(Constant.SESSION_USER_ORG_KEY, OrgInfo.dao.findById(user.getOrgId()));
			return user;
		}
		
		return null;
	}

        /**
         *权限控制规则扩展，返回null代表不扩展，扩展的是在plum.ini中的roles列表基础上在扩展
         */
	@Override
	public Map<String, Set<String>> roleMapExt() {
		Map<String,Set<String>> map = new HashMap<String,Set<String>>();
		List<Record> list = startPluginAndGetRef();
		if(list!= null && !list.isEmpty()){
	        for(Record r : list){
	        	String url = r.getStr("URL");
	        	String roleId = r.getStr("ROLE_ID");
	        	if(map.containsKey(url)){
	        		Set<String> set = map.get(url);
	        		if(!set.contains(roleId)){
	        			set.add(roleId);
	        		}
	        	}else{
	        		Set<String> set = new HashSet<String>();
	        		set.add(roleId);
	        		//添加admin权限访问
	        		set.add("admin");
	        		map.put(url,set);
	        	}
	        }}
		return map;
	}

        /**
         *设置当前用户拥有的权限，return null 代表当前登陆用户没有任何权限
         */
	@Override
	public Set<String> setHaveRoles() {
                //得到当前登陆用户
		Object obj = PlumSecurity.getUser();
		if(obj==null)
			return null;
		UserInfo user = (UserInfo)obj;
		String roles = user.getRoleIds();
		if(StrKit.isBlank(roles))
			return null;
		return new HashSet<String>(Arrays.asList(roles.split(",")));
	}

	/**
	 * 从数据库查询到权限菜单关系
	 * @return
	 */
	 public static List<Record> startPluginAndGetRef() {
				Prop init = PropKit.use("init.properties");
				//是否正式环境，true为正式环境，false为测试环境，默认为true
				String path = init.getBoolean("Env",true)?"production":"dev";
				Prop jdbc = PropKit.use(File.separator +path+File.separator+ "jdbc_oracle.properties");
				// oracle
				DruidPlugin druidPlugin = new DruidPlugin(jdbc.get("master.url"), jdbc.get("master.username"), jdbc.get("master.password").trim(), jdbc.get("driver"));
				ActiveRecordPlugin masterArpOracle = new ActiveRecordPlugin(druidPlugin);
				masterArpOracle.setContainerFactory(new CaseInsensitiveContainerFactory());
				masterArpOracle.setDialect(new OracleDialect());
				druidPlugin.start();
				Prop config = PropKit.use(path+File.separator+"config.properties");
				masterArpOracle.setShowSql(config.getBoolean("DevMode"));
				masterArpOracle.start();
				//arp.start();
				List<Record> list = Db.find("select RM.ROLE_ID,m.URL from ROLE_MENU rm,MENU m where RM.MENU_ID = m.ID and m.STATUS = ?","normal");
				 
				masterArpOracle.stop();
				druidPlugin.stop();
				return list;
			}

}

```


 **接下来就可以使用和测试了 :blush: ** 

 **plum QQ交流群:698042629** 

### 说明
1.当前项目还是一个初版，需要不断优化和完善，也请大家多提一些意见，一定会认真思考








